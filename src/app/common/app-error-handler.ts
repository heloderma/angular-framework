// Unexpected
// 1. Server is offline
// 2. Network is down
// 3. Unhandled exceptions

// Expected
// 1. Not found (404)
// 2. Bad Request(400)

import { ErrorHandler } from '@angular/core';

export class AppErrorHandler implements ErrorHandler {
  handleError(error) {
    alert('An unexpected error occured.');
    console.log(error);
  }
}

//LOCAL ERROR HANDLERS
export class AppError {
  constructor(public originalError?: any) {}
}

// error 0
export class UnavailableError extends AppError {}

//error 400
export class BadInputError extends AppError {}

//error 404
export class NotFoundError extends AppError {}

//error 500
export class InternalServerError extends AppError {}
