import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { AppErrorHandler } from './common/app-error-handler';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [AppComponent, HomeComponent, NavbarComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // 0. To work with Forms or Reactive Forms we need to add
    // 0.1 The validations will be placed in the common folder and interact with the data
    FormsModule,
    ReactiveFormsModule,
    // 1. To use Http we need to add HttpClientModule
    HttpClientModule,
    // 2. We use HttpClient in the Service (DataService) we will perform a http request
    // 3. Register the service created in [providers]
    // 4. Manejo de errores unexpected/Expected
    // 6.2 Ref.
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
  ],
  providers: [
    // 3.1 When we create a Service which extends of DAtaService we register it here
    // 5. Instead use ErrorHandler we will use our own herror handler class
    { provide: ErrorHandler, useClass: AppErrorHandler },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

// 6. If we will be using Firebase {npm install firebase @angular/fire --save | npm install -g firebase-tools}
// 6.1 Add in the environments file the configuration
// 6.2 Register the firebase options in imports[] from applicationCache.mudule

/**
 * HOW TO ADD
 * I Bootstrap
 * II Animations
 */

/**
 * HOW TO INSTALL: BOTSTRAP V4
 * 1. npm install bootstrap jquery @popperjs/core
 * 2. Add to the angular.json this
 * "styles": [
   "node_modules/bootstrap/dist/css/bootstrap.min.css",
   "src/styles.css"
],
"scripts": [
  "node_modules/jquery/dist/jquery.min.js",
  "node_modules/@popperjs/core/dist/umd/popper.min.js",
  "node_modules/bootstrap/dist/js/bootstrap.min.js"
]
 */
