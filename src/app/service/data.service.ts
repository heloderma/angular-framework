import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { throwError } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import {
  BadInputError,
  AppError,
  NotFoundError,
} from '../common/app-error-handler';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private url: string, private http: HttpClient) {}

  private static handleError(error: HttpErrorResponse) {
    if (error.status === 400) {
      return throwError(new BadInputError());
    }

    if (error.status === 404) {
      return throwError(new NotFoundError(error));
    }

    return throwError(new AppError(error));
  }

  getAll() {
    return this.http.get(this.url).pipe(
      map((response) => response),
      catchError(DataService.handleError)
    );
  }

  getByName(resource) {
    return this.http.get<any>(this.url + '/' + resource).pipe(
      map((response) => response),
      catchError(DataService.handleError)
    );
  }

  getByID(id) {
    return this.http.get<any>(this.url + '/' + id).pipe(
      map((response) => response),
      catchError(DataService.handleError)
    );
  }

  create(resource) {
    //Simular Error
    // return throwError(new AppError());
    return this.http.post(`${this.url}`, resource).pipe(
      map((response) => response),
      catchError(DataService.handleError)
    );
  }

  update(resource) {
    return this.http
      .patch(`${this.url}/${resource.id}`, JSON.stringify({ resource }))
      .pipe(
        map((response) => response),
        catchError(DataService.handleError)
      );
  }

  delete(id) {
    return this.http.delete(this.url + '/' + id).pipe(
      retry(3),
      map((response) => response),
      catchError(DataService.handleError)
    );
  }
}

// 1. How to create an objet of this class

/**
  export class PostService extends DataService {
  constructor(http: HttpClient) {
    super('url', http);
  }
}
 */
