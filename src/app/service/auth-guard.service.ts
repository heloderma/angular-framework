import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
// 6. Read more abour Rout guard and how to register into providers
// array in applicationCache.module in the file['app-routing.module']
export class AuthGuard implements CanActivate {
  constructor(private router: Router /**, service:ServieofType */) {}

  canActivate(route, state: RouterStateSnapshot) {
    // if (this.service.isLoggedIn()) {
    //   return true;
    // } else {
    //   this.router.navigate(['/login'], {
    //     queryParams: { returnUrl: state.url },
    //   });
    return false;
    // }
  }
}
