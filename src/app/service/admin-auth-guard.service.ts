import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
// 6. Read more abour Rout guard and how to register into providers
// array in applicationCache.module in the file['app-routing.module']
export class AdminAuthGuard {
  constructor(
    private router: Router // private authService: AuthService
  ) {}

  canActivate() {
    // const user = this.authService.CurrentUser;
    // if (user && user.admin) {
    //   return true;
    // }

    // this.router.navigate(['/no-access']);
    return false;
  }
}
